# Height Map

Now installable as an extension on the OpenFlexure OS Image.

Using a flat sample with enough features to focus on, recentre the stage relative to the objective to maximise the range of motion.

## Installing

This is an extension for the OpenFlexure microscope server.

For now, you should clone this repository, and symlink the `recentre` folder into your plugins folder, usually `/var/openflexure/extensions/microscope_extensions/`.  If you are running the default OpenFlexure Raspbian image, that can be accomplished with the following commands:

```console
cd
git clone https://gitlab.com/jaknapper/height_map.git
sudo ln -s `pwd`/height_map/recentre /var/openflexure/extensions/microscope_extensions/
sudo ofm restart
```

## Using

A tab should appear in the microscope GUI allowing you to set the objective alignment (scroll down) and run a recentring procedure.  The stage doesn't need to start near the centre, but avoid it being within a couple of mm of the objective in any direction.  As well as locating the stage at the estimated centre point (which you can then set to (0,0,0) from the Navigate tab), it will save a JSON of all recorded locations for plotting in `/var/openflexure/heightmap_full.json`.

Adding this in to the scan routine is a work in progress.