import inspect
import logging
from contextlib import contextmanager
from typing import Callable, Dict, List, Optional, Tuple, cast

from labthings import current_action, fields, find_component
from labthings.utilities import get_docstring, get_summary
from labthings.views import ActionView, PropertyView, View
from scipy import ndimage

def extension_action(args=None):
    """A decorator to auto-create an Action endpoint for a method
    
    Use this decorator on any method of an extension (`BaseExtension` subclass)
    and it will automatically be added to the API.  At present it is deliberately
    basic, and the plan is to expand the options in the future.
    
    Currently, you may specify `args` (which is a Marshmallow-format schema
    or dictionary, determining the datatype of the arguments, which will be
    taken from the JSON payload of the POST request initiating the action).
    
    The parsed arguments dictionary is expanded as the function arguments,
    i.e. we call `decorated_method(self, **kwargs)`.  This means that any
    unused arguments will cause an error, which is probably good practice...

    NB this decorator does **not** replace the function with a `View` or 
    register it with the parent `Extension`.  It adds the created `View` as 
    a property of the function, `flask_view`.  The parent `Extension` is
    responsible for collating and adding the views in its `__init__` method.
    """
    supplied_args = args

    def decorator(func):
        class_docstring = f"""Manage actions for {func.__name__}.
        
        This `View` class will return a list of `Action` objects representing
        each time {func.__name__} has been run in response to a `GET` request,
        and will start a new `Action` in response to a `POST` request.
        """

        class ActionViewWrapper(ActionView):
            __doc__ = class_docstring
            args = supplied_args

            def post(self, arguments=None):
                # Run the action
                if arguments == None:
                    arguments = {}
                return func(self.extension, **arguments)

            def get(self, *args, **kwargs):  # pylint: disable=useless-super-delegation
                # Explicitly wrap the `get` method to allow us to add a docstring
                return super().get(*args, **kwargs)

        # Create a nice docstring.  NB because the source function and this docstring
        # aren't guaranteed to have the same leading whitespace, we just make sure
        # both docstrings are stripped of leading indent, using `inspect.cleandoc()`
        ActionViewWrapper.post.description = (
            get_docstring(func, remove_newlines=False)
            + "\n\n"
            + inspect.cleandoc(
                """
                This `POST` request starts an Action, i.e. the hardware will do something
                that may continue after the HTTP request has been responded to.  The 
                response will always be an Action object, that details the current 
                status of the action and provides an interface to poll for completion.
                
                If the action completes within a specified timeout, we will return
                an HTTP status code of `200` and the return value will include any
                output from the action.  If it does not complete, we will return a
                `201` response code, and the action's endpoint may be polled to follow
                its progress.
                """
            )
        )
        ActionViewWrapper.post.summary = get_summary(func)
        ActionViewWrapper.post.__doc__ = ActionViewWrapper.post.description
        ActionViewWrapper.__name__ = func.__name__
        ActionViewWrapper.get.summary = (
            f"List running and completed `{func.__name__}` actions."
        )
        ActionViewWrapper.get.description = (
            ActionViewWrapper.get.summary
            + "\n\n"
            + inspect.cleandoc(
                f"""
                This `GET` request will return a list of `Action` objects corresponding
                to the `{func.__name__}` action.  It will include all the times it has
                been run since the server was last restarted, including running, completed,
                and failed attempts.
                """
            )
        )

        func.flask_view = ActionViewWrapper
        return func

    return decorator


def extension_property(schema=None):
    """A decorator to auto-create a Property endpoint for a method
    
    Use this decorator on any method of an extension (`BaseExtension` subclass)
    and it will automatically be added to the API.  At present it is deliberately
    basic, and the plan is to expand the options in the future.
    
    Currently, you may specify `schema` (which is a Marshmallow-format schema
    or dictionary, determining the datatype of the property's value).
    
    NB this decorator does **not** replace the function with a `View` or 
    register it with the parent `Extension`.  It adds the created `View` as 
    a property of the function, `flask_view`.  The parent `Extension` is
    responsible for collating and adding the views in its `__init__` method.
    """
    supplied_schema = schema
    def decorator(func):
        class_docstring = f"""Manage the value of the  {func.__name__} property.
        
        This `View` class will return the value of the {func.__name__} 
        property in response to a GET request.  Setting in response to a 
        POST request is not yet implemented.
        """

        class PropertyViewWrapper(PropertyView):
            __doc__ = class_docstring
            schema = supplied_schema

            def get(self, *args, **kwargs):  # pylint: disable=useless-super-delegation
                # Return the value of the getter function (PropertyView takes care of serialisation)
                return func(self.extension, *args, **kwargs)

        # Create a nice docstring.  NB because the source function and this docstring
        # aren't guaranteed to have the same leading whitespace, we just make sure
        # both docstrings are stripped of leading indent, using `inspect.cleandoc()`
        PropertyViewWrapper.get.description = (
            get_docstring(func, remove_newlines=False)
            + "\n\n"
            + inspect.cleandoc(
                """
                This `GET` request returns the value of the property.  A `POST` request,
                if supported, will set the value.
                """
            )
        )
        PropertyViewWrapper.get.summary = get_summary(func)
        PropertyViewWrapper.get.__doc__ = PropertyViewWrapper.get.description
        PropertyViewWrapper.__name__ = func.__name__

        func.flask_view = PropertyViewWrapper
        return func

    return decorator
