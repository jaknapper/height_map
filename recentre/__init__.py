# import matplotlib.pyplot as plt
import numpy as np
# import openflexure_microscope_client
import json
import pprint
import logging


import numpy as np
from flask import current_app as app
from labthings import current_action, fields, find_component
from labthings import find_component, find_extension
from labthings.extensions import BaseExtension
from labthings.find import current_labthing, registered_extensions
from labthings.utilities import path_relative_to
from openflexure_microscope.api.utilities.gui import build_gui
from openflexure_microscope.api.default_extensions.autofocus import AutofocusExtension, find_microscope
from .labthings_extension_decorators import extension_action, extension_property


def _gui_description():
    return {
        "icon": "center_focus_weak",
        "forms": [
            {
                "name": "Run recentre",
                "route": "/recentre",
                "submitLabel": "Run recentre",
                "isTask": True,
                "schema": [
                    {
                        "fieldType": "htmlBlock",
                        "name": "status_display",
                        "label": "current_status",
                        "content": (
                            "Find the centre of the range of motion of the microscope, based on the position of the highest point."
                            "<br />"
                            "<br />"
                            "Requires a flat sample with fairly dense features to focus on."
                        )
                    },
                ]
            },
            {
                "name": "Set objective orientation",
                "route": "/set_objective_orientation",
                "submitLabel": "Set orientation",
                "isTask": False,
                "schema": [
                    {
                        "fieldType": "selectList",
                        "name": "orientation",
                        "label": "Objective orientation",
                        "options": ["Up", "Down"],
                        "value": get_objective_orientation()
                    },
                ]
            }
        ]
    }

def turningpoints(lst):
    dx = np.diff(lst)
    return (dx[1:] * dx[:-1] < 0)

def looping_autofocus(microscope):
    repeat = True
    dz=2000
    while repeat:
        # Locate the autofocus extension
        autofocus_extension = find_extension("org.openflexure.autofocus")
        data = autofocus_extension.fast_autofocus(microscope, dz)
        heights, sizes = unpack_autofocus(data)
        if microscope.stage.position[2] - min(heights) < dz / 5 or max(heights) - microscope.stage.position[2] < dz / 5:
            pass
        else:
            repeat = False

def get_objective_orientation() -> str:
    microscope = find_component("org.openflexure.microscope")
    current_config = microscope.configuration_file.load()
    try:
        if current_config['objective']['orientation'] in ['Up', 'Down']:
            return current_config['objective']['orientation']
        else:
            raise Exception
    except:
        return None

def unpack_autofocus(scan_data):
    """Extract z, sharpness data from a move_and_measure call
    
    Data will start at `start_index`, i.e. `start_index` points are dropped
    from the beginning of the array.
    """
    jpeg_times = scan_data['jpeg_times']
    jpeg_sizes = scan_data['jpeg_sizes']
    jpeg_sizes_MB = [x / 10**3 for x in jpeg_sizes]
    stage_times = scan_data['stage_times']
    stage_positions = scan_data['stage_positions']
    stage_height = [pos[2] for pos in stage_positions]

    jpeg_heights = np.interp(jpeg_times,stage_times,stage_height)

    turning = np.where(turningpoints(jpeg_heights))[0]+1

    return jpeg_heights[turning[0]:turning[1]], jpeg_sizes_MB[turning[0]:turning[1]]


# Create the extension class
class RecentreExtension(BaseExtension):
    _commands = {}

    def __init__(self):
        super().__init__(
            "org.openflexure.recentre",
            version="0.0.1",
            static_folder=path_relative_to(__file__, "static"),
        )

        # This should be replaced with a labthings import in due course
        AutofocusExtension.add_decorated_method_views(self)
        self.add_meta("gui", build_gui(self.gui, self))

    def gui(self):
        return _gui_description()

    @extension_action(args={
        "orientation": fields.String()
    })
    def set_objective_orientation(self, orientation):
        microscope = find_component("org.openflexure.microscope")
        current_config = microscope.configuration_file.load()
        if 'objective' not in current_config.keys():
            current_config['objective'] = {}
        current_config['objective']['orientation'] = orientation
        microscope.configuration_file.save(current_config)

    @extension_action(
        args={}
    )
    def recentre(self):#, max_steps = 15, lateral_distance = 5000):
        max_steps = 15
        dx = 5000
        microscope = find_microscope()

        objective_alignment = get_objective_orientation()

        # This is based on finding the highest position of the stage
        # For an upright microscope, that's the lowest point so we flip it
        if objective_alignment == 'Up':
            logging.info("We're trying to find the position of maximum height")
            height_inversion = 1
        elif objective_alignment == 'Down':
            logging.info("We're trying to find the position of minimum height")
            height_inversion = -1
        else:
            logging.warning("We don't know the orientation of the objective. Assuming pointing up (default)")
            height_inversion = 1


        centre = list(microscope.stage.position)

        # A list of all the positions we've focused
        focused_pos = [[],[]]

        looping_autofocus(microscope)

        for direction in [0, 1]:
            # Start off with the current position, and moving in the positive direction
            focused_pos[direction] = [list(microscope.stage.position)]
            moves = +1
            
            
            microscope.stage.move_abs(centre)
            steps = 0
            all_heights = []

            # We'll run this for x, then y
            while True:
                # If we're moving in the positive direction, we want the highest point
                # Otherwise, we want the lowest
                if moves > 0:
                    starting_point = np.max(np.array(focused_pos[direction])[:,direction])
                else:
                    starting_point = np.min(np.array(focused_pos[direction])[:,direction])
                
                # Next location is an extra move in the direction we want
                destination = centre
                destination[direction] = starting_point + moves * dx
                microscope.stage.move_abs(destination)
                looping_autofocus(microscope)

                position = list(microscope.stage.position)            
                focused_pos[direction].append(position)

                logging.warning(focused_pos)

                # If we've been moving in positive direction, and height has decreased,
                # we want to turn around
                if position[2]*height_inversion < focused_pos[direction][-2][2]*height_inversion:
                    moves *= -1

                steps += 1
                if steps > max_steps:
                    break
                if len(focused_pos[direction]) > 2:
                    # If the highest position isn't 
                    # if np.argmax(np.array(focused_pos[direction])[:,2]) != np.argmax(np.array(focused_pos[direction])[:,direction]) and np.argmax(np.array(focused_pos[direction])[:,direction]) != np.argmax(np.array(focused_pos[direction])[:,2]):
                    
                    all_heights = [x[2]*height_inversion for x in focused_pos[direction]]
                    direction_index = [x[direction] for x in focused_pos[direction]]
                    
                    sorted_all_heights = [x for y, x in sorted(zip(direction_index, all_heights))]
                    
                    logging.warning(sorted_all_heights)
                    if np.argmax(sorted_all_heights) != 0 and np.argmax(sorted_all_heights) != len(all_heights) - 1:
                        print(f'Breaking because the highest point is at {np.argmax(np.array(focused_pos[direction])[:,2])} in the list')
                        break
            
            # Centre value is replaced by the maximum value recorded in that axis
            centre[direction] = focused_pos[direction][np.argmax(all_heights)][direction]
            microscope.stage.move_abs(centre)
            looping_autofocus(microscope)

        print(f"Centre of ROM is at {centre, microscope.stage.position[2]} \n")

        import json

        with open('heightmap_full.json', 'w') as f:
            json.dump(focused_pos, f)

        return focused_pos

        # to_plot = focused_pos.copy()
        # to_plot = [[e for sl in to_plot for e in sl]]

        # print(to_plot[0])

        # to_plot_x = np.array([loc[0] for loc in to_plot[0]])
        # to_plot_y = np.array([loc[1] for loc in to_plot[0]])
        # to_plot_z = np.array([loc[2] for loc in to_plot[0]])

        # to_plot_all = np.array(to_plot[0])

        # cols = 2
        # rows = 1


        # fig = plt.figure(figsize=(12, 3))
        # import matplotlib.gridspec as gridspec
        # G = gridspec.GridSpec(rows, cols)


        # ax2 = fig.add_subplot(G[rows-1,0])
        # ax2.plot(to_plot_x, to_plot_z,'.')

        # ax3 = fig.add_subplot(G[rows-1,1])
        # ax3.plot(to_plot_y, to_plot_z,'.')
        # ax2.set_xlabel('Stage position x (steps)', fontsize = 16)
        # ax2.set_ylabel('Stage position\nz (steps)', fontsize = 16)
        # ax3.set_xlabel('Stage position y (steps)', fontsize = 16)
        # plt.tight_layout()
        # plt.show()

        # import plotly.graph_objects as go

        # fig = go.Figure(data=[go.Scatter3d(x=to_plot_x, y=to_plot_y, z=to_plot_z,
        #                                 mode='markers')])

        # fig.show()

        # x_coords = np.array(focused_pos[0])
        # y_coords = np.array(focused_pos[1])

        # quad_params = []

        # x = x_coords[:, 0]
        # x_new = np.linspace(min(x), max(x), 500)
        # quad_params.append(np.polyfit(x, x_coords[:, 2], 2))
        # f = np.poly1d(quad_params[-1])
        # y_new = f(x_new)

        # plt.plot(x_new, y_new, '.')
        # plt.plot(x, x_coords[:, 2], '.', color = 'red')
        # plt.show()
        # x_centre = int(x_new[np.argmin(y_new)])

        # x = y_coords[:, 1]
        # x_new = np.linspace(min(x), max(x), 500)
        # quad_params.append(np.polyfit(x, y_coords[:, 2], 2))
        # f = np.poly1d(quad_params[-1])
        # y_new = f(x_new)

        # plt.plot(x_new, y_new, '.')
        # plt.plot(x, y_coords[:, 2], '.', color = 'red')
        # plt.show()
        # y_centre = int(x_new[np.argmin(y_new)])

        # z_max = np.min(np.array(focused_pos[1])[:,2])

        # microscope.move([int(x_centre),int(y_centre),int(z_max)])

        # looping_autofocus(microscope)

        # parasite_results = {
        #     'centre' : {
        #     'x' : int(x_centre),
        #     'y' : int(y_centre),
        #     'z' : int(z_max) 
        #     },
        #     'x_fit' :{
        #     'a' : quad_params[0][0],
        #     'b' : quad_params[0][1],
        #     'c' : quad_params[0][2],
        #     },
        #     'y_fit' :{
        #     'a' : quad_params[1][0],
        #     'b' : quad_params[1][1],
        #     'c' : quad_params[1][2],
        #     }
        # }

        # return parasite_results


LABTHINGS_EXTENSIONS = (RecentreExtension,)
