# import matplotlib.pyplot as plt
import numpy as np
import openflexure_microscope_client
import sys

def turningpoints(lst):
    dx = np.diff(lst)
    return (dx[1:] * dx[:-1] < 0)

def looping_autofocus(microscope):
    repeat = True
    dz=2000
    while repeat:
        data = microscope.autofocus()
        heights, sizes = unpack_autofocus(data)
        if microscope.position['z'] - min(heights) < dz / 10 or max(heights) - microscope.position['z'] < dz / 10:
            pass
        else:
            repeat = False
            
def unpack_autofocus(scan_data):
    """Extract z, sharpness data from a move_and_measure call
    
    Data will start at `start_index`, i.e. `start_index` points are dropped
    from the beginning of the array.
    """
    jpeg_times = scan_data['jpeg_times']
    jpeg_sizes = scan_data['jpeg_sizes']
    jpeg_sizes_MB = [x / 10**3 for x in jpeg_sizes]
    stage_times = scan_data['stage_times']
    stage_positions = scan_data['stage_positions']
    stage_height = [pos[2] for pos in stage_positions]

    jpeg_heights = np.interp(jpeg_times,stage_times,stage_height)

    turning = np.where(turningpoints(jpeg_heights))[0]+1

    return jpeg_heights[turning[0]:turning[1]], jpeg_sizes_MB[turning[0]:turning[1]]

def looping_autofocus(microscope):
    repeat = True
    dz=2000
    while repeat:
        data = microscope.autofocus()
        heights, sizes = unpack_autofocus(data)
        if microscope.position['z'] - min(heights) < dz / 10 or max(heights) - microscope.position['z'] < dz / 10:
            pass
        else:
            repeat = False

def locate_peak(microscope, step_size):
    max_steps = 29
    dx = step_size

    focused_pos = []
    centre = list(microscope.position.values())

    focused_pos = [[],[]]

    looping_autofocus(microscope)

    for direction in [0, 1]:
        focused_pos[direction] = [list(microscope.position.values())]
        moves = +1
        dx = dx
        microscope.move(centre)
        for i in range(max_steps):
            if moves > 0:
                starting_point = np.max(np.array(focused_pos[direction])[:,direction])
            else:
                starting_point = np.min(np.array(focused_pos[direction])[:,direction])
            
            destination = centre
            destination[direction] = starting_point + moves * dx

            microscope.move(destination)
            position = list(microscope.position.values())

            x_diff = position[0]-centre[0]
            y_diff = position[1]-centre[1]
            r = np.sqrt(x_diff**2+y_diff**2)
            z_diff = int(-1.3 * 10**-6 * r**2 + 3.5 * 10**-2 * r)

            microscope.move_rel([0,0,z_diff])

            looping_autofocus(microscope)
            focused_pos[direction].append(list(microscope.position.values()))

            if microscope.position['z'] < focused_pos[direction][-2][2]:
                moves *= -1

            print(focused_pos[direction])
            print(np.argmin(np.array(focused_pos[direction])[:,2]), np.argmax(np.array(focused_pos[direction])[:,direction]), np.argmin(np.array(focused_pos[direction])[:,direction]))

            if len(focused_pos[direction]) > 2:
                if np.argmax(np.array(focused_pos[direction])[:,2]) != np.argmin(np.array(focused_pos[direction])[:,direction]) and np.argmax(np.array(focused_pos[direction])[:,direction]) != np.argmax(np.array(focused_pos[direction])[:,2]):
                    print(f'Breaking because the lowest point is at {np.argmin(np.array(focused_pos[direction])[:,2])} in the list')
                    break
        
        centre[0] = focused_pos[0][np.argmax(np.array(focused_pos[0])[:,2])][0]
    return focused_pos

def locate_centre(focused_pos, microscope):
    x_coords = np.array(focused_pos[0])
    y_coords = np.array(focused_pos[1])

    quad_params = []

    x = x_coords[:, 0]
    x_new = np.linspace(min(x), max(x), 500)
    quad_params.append(np.polyfit(x, x_coords[:, 2], 2))
    f = np.poly1d(quad_params[-1])
    y_new = f(x_new)

    x_centre = int(x_new[np.argmax(y_new)])

    x = y_coords[:, 1]
    x_new = np.linspace(min(x), max(x), 500)
    quad_params.append(np.polyfit(x, y_coords[:, 2], 2))
    f = np.poly1d(quad_params[-1])
    y_new = f(x_new)

    y_centre = int(x_new[np.argmax(y_new)])

    z_max = np.max(np.array(focused_pos[1])[:,2])

    microscope.move([x_centre, y_centre, z_max])
    return x_centre, y_centre, z_max

if __name__ == "__main__":
    ip = sys.argv[1]
    microscope = openflexure_microscope_client.MicroscopeClient(ip)
    focused_pos = locate_peak(microscope, 5000)
    x_centre, y_centre, z_max = locate_centre(focused_pos, microscope)

    focused_pos = locate_peak(microscope, 1000)
    x_centre, y_centre, z_max = locate_centre(focused_pos, microscope)

    print(f'Estimated location of centre of ROM is {x_centre}, {y_centre}. Approx stage height here is {z_max}')
# to_plot = focused_pos.copy()
# to_plot = [[e for sl in to_plot for e in sl]]

# print(to_plot[0])

# to_plot_x = np.array([loc[0] for loc in to_plot[0]])
# to_plot_y = np.array([loc[1] for loc in to_plot[0]])
# to_plot_z = np.array([loc[2] for loc in to_plot[0]])

# to_plot_all = np.array(to_plot[0])

# # %% [markdown]
# # Plot the trends in x and y. Should see a peak in each

# # %%
# cols = 2
# rows = 1


# fig = plt.figure(figsize=(12, 3))
# import matplotlib.gridspec as gridspec
# G = gridspec.GridSpec(rows, cols)


# ax2 = fig.add_subplot(G[rows-1,0])
# ax2.plot(to_plot_x, to_plot_z,'.')

# ax3 = fig.add_subplot(G[rows-1,1])
# ax3.plot(to_plot_y, to_plot_z,'.')
# ax2.set_xlabel('Stage position x (steps)', fontsize = 16)
# ax2.set_ylabel('Stage position\nz (steps)', fontsize = 16)
# ax3.set_xlabel('Stage position y (steps)', fontsize = 16)
# plt.tight_layout()
# plt.show()

# import plotly.graph_objects as go

# fig = go.Figure(data=[go.Scatter3d(x=to_plot_x, y=to_plot_y, z=to_plot_z,
#                                    mode='markers')])

# fig.show()

# # %% [markdown]
# # Fit a quadratic to the z movement against both x and y seperately

# # %%
# x_coords = np.array(focused_pos[0])
# y_coords = np.array(focused_pos[1])

# quad_params = []

# x = x_coords[:, 0]
# x_new = np.linspace(min(x), max(x), 500)
# quad_params.append(np.polyfit(x, x_coords[:, 2], 2))
# f = np.poly1d(quad_params[-1])
# y_new = f(x_new)

# plt.plot(x_new, y_new, '.')
# plt.plot(x, x_coords[:, 2], '.', color = 'red')
# plt.show()
# x_centre = int(x_new[np.argmax(y_new)])

# x = y_coords[:, 1]
# x_new = np.linspace(min(x), max(x), 500)
# quad_params.append(np.polyfit(x, y_coords[:, 2], 2))
# f = np.poly1d(quad_params[-1])
# y_new = f(x_new)

# plt.plot(x_new, y_new, '.')
# plt.plot(x, y_coords[:, 2], '.', color = 'red')
# plt.show()
# y_centre = int(x_new[np.argmax(y_new)])

# z_max = np.max(np.array(focused_pos[1])[:,2])

# microscope.move([x_centre, y_centre, z_max])

# # %% [markdown]
# # Update the microscope settings folder with these estimations

# # %%
# import requests

# def put_json(self, path, payload = {}):
#     if not path.startswith("http"):
#         path = self.base_uri + path
#     r = requests.put(path, json = payload)
#     r.raise_for_status()
#     r = r.json()
#     return r

# def pull_settings(self):
#     """Download the current microscope settings as a JSON"""
#     return self.get_json("/instrument/settings")

# def post_settings(self, params):
#     """Update the microscope settings file with params"""
#     return put_json(microscope, "/instrument/settings", params)

# # %%
# settings = pull_settings(microscope)

# settings['extensions']['org.openflexure.self_centred'] = {
#     'centre' : {
#     'x' : int(x_centre),
#     'y' : int(y_centre),
#     'z' : int(z_max) 
#     },
#     'x_fit' :{
#     'a' : quad_params[0][0],
#     'b' : quad_params[0][1],
#     'c' : quad_params[0][2],
#     },
#     'y_fit' :{
#     'a' : quad_params[1][0],
#     'b' : quad_params[1][1],
#     'c' : quad_params[1][2],
#     }
# }

# post_settings(microscope, settings)

# pprint.pprint(settings['extensions']['org.openflexure.self_centred'])

# # %% [markdown]
# # Plot the x vs z fit as a sanity check

# # %%
# centre = [settings['extensions']['org.openflexure.self_centred']['centre']['x'], settings['extensions']['org.openflexure.self_centred']['centre']['y'], settings['extensions']['org.openflexure.self_centred']['centre']['z']] 
# x_fit = [settings['extensions']['org.openflexure.self_centred']['x_fit']['a'], settings['extensions']['org.openflexure.self_centred']['x_fit']['b'], settings['extensions']['org.openflexure.self_centred']['x_fit']['c']]

# x = np.linspace(-1000,10000,10000) + centre[0]

# f = np.poly1d(np.array(x_fit))
# z = f(x)

# plt.plot(x, z)
# plt.show()


